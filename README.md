# NextJS + Express Portofolio
>This is my web-based portofolio using NextJS(CNA) + Express (will be deployed using docker + MERN stack)

## To Deploy

First, make sure mongoDB is started and then create `.env` file using the examples.

After that, just run the server, make sure to choose dev (using nodemon) or prod

```bash
npm run prod
#or
npm run dev-start
```

Open http://localhost:node_port with your browser to see the web.