require('dotenv').config()

const next = require('next')
const express = require('express')
const mongoose = require('mongoose')
const bodyparser = require('body-parser')

const nextOptions = {
    dev: true
}

const app = next(nextOptions)
const handle = app.getRequestHandler()

app.prepare().then(() => {
    const server = express()

    server.get('/', (req, res) => {
        return app.render(req, res, '/', req.query)
    })

    server.all('*', (req, res) => {
        return handle(req, res)
    })

    server.listen(process.env.NODE_PORT, (err) => {
        if (err) throw err
        console.log('server started!')
    })
})